#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime
from typing import Optional

from config.models import Cdb
from profiles.models import ProfileType
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.POSTS


class PostBase(BaseModel):
    """
    Base
    """

    source_ip: str = None  # ip address post came from
    profile_id: str = None  # profile the post belongs to
    profile_type: ProfileType = None  # profile type


class PostExt(PostBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    # updated: datetime = datetime.now()

    creator: Optional[str] = None
    # updator: Optional[str] = None


class Post(PostExt):
    """
    Actual (at DB level)
    """

    id_: str


class PostResponse(Post):
    """
    Response
    """

    pass


class StandardFormResponse(BaseModel):
    """
    Standard form response.
    """

    mid: str = None  # the message id
    message: str = None  # the message
    success: bool = False  # success (True|False)


class ProfileCountResponse(BaseModel):
    """
    Profile Count response.
    """

    success: bool = False  # success (True|False)
    count: int = 0  # count of posts for given profile
