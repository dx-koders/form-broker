#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import asyncio
import logging

from arq import create_pool
from arq.connections import RedisSettings
from config.config import (  # noqa: F401
    REDIS_DB_ARQ,
    REDIS_HOST,
    REDIS_PASSWORD,
    REDIS_PORT,
    couchdb,
    queue,
)
from fastapi import APIRouter, Form, Query, Request, Security
from posts.models import ProfileCountResponse, StandardFormResponse, _db
from users.models import UserBase, get_current_user
from redis.exceptions import RedisError

logger = logging.getLogger("default")
posts_router = APIRouter()


@posts_router.post("/form/{access_key}", response_model=StandardFormResponse)
async def form(
    request: Request,
    access_key: str,
    name: str = Form(...),
    email: str = Form(...),
    subject: str = Form(...),
    message: str = Form(...),
):
    global queue

    data = {
        "name": name,
        "email": email,
        "subject": subject,
        "message": message,
        "source_ip": request.client.host,
    }
    logger.debug(f"- received submital for access_key={access_key} of: {data}")

    try:
        # write message to worker
        if not queue:
            logger.info("No Queue connection found, establishing...")
            queue = await create_pool(
                RedisSettings(
                    host=REDIS_HOST,
                    port=REDIS_PORT,
                    database=REDIS_DB_ARQ,
                    password=REDIS_PASSWORD,
                )
            )
        else:
            logger.debug(f"- existing Queue connection found: {queue}")

        # note: delay job by a few seconds to allow save time.
        job = await queue.enqueue_job("coordinator", access_key, data, _defer_by=1)
        logger.info(f"NOTICE: Submission scheduled for processing via Worker with job id: {job.job_id}")

        return StandardFormResponse(success=True, mid=job.job_id, message="message received")

    except (ConnectionError, OSError, RedisError, asyncio.TimeoutError) as e:
        logger.error(f"Got connection error: {e}")
        return StandardFormResponse(
            success=False,
            mid=None,
            message=f"Queue connection error, is queue up? More detail: {e}",
        )
    except Exception as e:
        logger.error(f"Got other error: {e}")
        return StandardFormResponse(success=False, mid=None, message=f"Other error, more detail: {e}")


@posts_router.get("/count/{profile_id}")
async def profile_count(
    profile_id: str,
    skip: int = Query(
        0,
        description="Skip number of records (useful if you already have records in profile - e.g. for load testing)",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get count for given profile.
    """

    logger.debug(f"- get count for profile_id: {profile_id}")
    db = await couchdb[_db.value]

    selector = {"profile_id": profile_id}
    logger.debug(f"- selector: {selector}")
    _cnt = 0
    async for doc in db.find(selector=selector, skip=skip):
        _cnt += 1

    return ProfileCountResponse(success=True, count=_cnt)
