#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
import uuid
from datetime import datetime

import aiocouch
from _common.utils import make_id
from config.config import couchdb
from fastapi.encoders import jsonable_encoder
from posts.models import PostExt, ProfileType, _db

logger = logging.getLogger("default")


async def audit_post(
    source_ip: str = None,
    profile_id: str = None,
    profile_type: ProfileType = None,
):
    """
    Add a message.
    """

    db = await couchdb[_db.value]

    _post = PostExt(
        source_ip=source_ip,
        profile_id=profile_id,
        profile_type=profile_type,
        created=datetime.now(),
    )

    # ~ logger.critical(
    # ~ f"Log Message Received at {log.created}: level={level}, status={status}, location={location}, message={message}"
    # ~ )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_post),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
