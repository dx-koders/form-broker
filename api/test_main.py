# test_main.py
#
# NOTICES:
#  - we can now run with: `pytest`, previously we needed to call as follows:
#    + run with: python -m pytest (as `pytest` does not work - path issues)
#      - normal: python -m pytest -ra --tb=short --no-header
#    + if you wanted detailed debug info (e.g. print statement output) you need to call with the `-rP` params (e.g. `python -m pytest -rP`).
#  - you need to set several envvars to run this test (see os.getenv) you can pass
#    these in when running the test (e.g. $ TEST_APP_USER_USERNAME=tester python -m pytest)
#    or (recommended approach) is to set them in the .env file so they will be
#    set in the app container
#  - if you wanted detailed debug info (e.g. print statement output) you need to call
#    with the `-rP` params (e.g. `python -m pytest -rP`).
#
# Test Process Summary
#  >> NOTE: tests are arranged in order of events not by endpoint <<
#  - create test user (also test me, get all, get one, update, and delete)
#  - create test profile under test user (also test get all, get one, update, and delete)
#  - create test dispatcher under test profile (also test get all, get all for profile, get one, update, and delete)
#  - perform test post under access_key for test dispatcher (also test post count per profile)
#  - test get all messages, get all for access key, get one, and delete
#  - cleanup created messages
#  - cleanup created dispatchers
#  - cleanup created profiles
#  - cleanup created users
#

import os
import pytest
import asyncio
import json
import uuid
from httpx import AsyncClient
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient
from main import app

runstamp = uuid.uuid4().hex
_test_app_user_username = os.getenv("TEST_APP_USER_USERNAME")
_test_app_user_password = os.getenv("TEST_APP_USER_PASSWORD")
_test_auth_user_username = os.getenv("TEST_AUTH_USER_USERNAME")
_test_auth_user_password = os.getenv("TEST_AUTH_USER_PASSWORD")

_test_dispatcher_email_server_url = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_URL")
_test_dispatcher_email_server_port = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_PORT")
_test_dispatcher_email_server_username = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_USERNAME")
_test_dispatcher_email_server_password = os.getenv("TEST_DISPATCHER_EMAIL_SERVER_PASSWORD")
_test_dispatcher_email_to = os.getenv("TEST_DISPATCHER_EMAIL_TO")


app_version = {
    "status": "OK",
    "version": "1.7.0",
    "created": "2021-12-06",
    "modified": "2024-12-09",
}
# test_app_user* is the user used when executing tests
test_app_user_for_update_username = "tester4update"
test_app_user = {
    "username": _test_app_user_username,
    "password": _test_app_user_password,
    "grant_type": "password",
}
test_app_user_for_update = {
    "display_name": "updated by pytest",
    "avatar": "https://dradux.com/avatar/tester4update",
    "note": f"updated by pytest: runstamp={runstamp}",
    "enabled": jsonable_encoder(False),
}
# user that tests authentication (as opposed to user used for running tests)
test_auth_user = {
    "username": _test_auth_user_username,
    "password": _test_auth_user_password,
    "grant_type": "password",
}
# profile used for test
test_profile = {
    "profile_type": "contact",
    "name": "PyTest Profile",
    "note": "Profile created by pytest for testing",
    "enabled": jsonable_encoder(True),
}
# db dispatcher used for test
test_dispatcher_db = {
    "dispatcher_type": "database",
    "name": "PyTest Dispatcher (db)",
    "note": "A test dispatcher created by pytest for testing db dispatching",
    "enabled": jsonable_encoder(True),
}
# email dispatcher used for test
test_dispatcher_email = {
    "dispatcher_type": "email",
    "name": "PyTest Dispatcher (email)",
    "note": "A test dispatcher created by pytest for testing email dispatching",
    "config": json.dumps(
        {
            "server_url": _test_dispatcher_email_server_url,
            "server_port": _test_dispatcher_email_server_port,
            "from": "pytest@fbkr.lan",
            "to": _test_dispatcher_email_to,
            "username": _test_dispatcher_email_server_username,
            "password": _test_dispatcher_email_server_password,
        }
    ),
    "enabled": jsonable_encoder(True),
}
# post used for test
test_post = {
    "name": "Py Tester",
    "email": "py@tester.com",
    "subject": "A PyTest Message",
    "message": "Test message created via pytest which should go to db and email!",
}

# _cleanup_pause: minimum 2 seconds, might need more if test_posts_count is failing
#   as the post is likely not persisting via worker fast enough.
_cleanup_pause = 2
PATH_SERVER = "http://localhost:8000"
PATH_BASE = "/api"
PATH_APP = f"{PATH_BASE}/v2"

test_profile_id = None  # used to store the test profile id
test_profile_access_key = None  # used to store test profile access_key
test_dispatcher_db_id = None  # used to store the test db dispatcher id
test_dispatcher_email_id = None  # used to store the test email dispatcher id
test_message_id = None  # used to store the test message id

headers_base = {"content-type": "application/json", "accept": "application/json"}
headers_form = {
    "content-type": "application/x-www-form-urlencoded",
    "accept": "application/json",
}

client = TestClient(app)


@pytest.fixture(scope="module")
async def get_auth_token():
    """
    Get Authentication Token
    - notes
      + the token is used throughout the remainder of the tests
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=test_app_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
    if response and response.json():
        token = response.json()["access_token"]
        return token
    else:
        pytest.exit(99)


async def auth_test():
    """
    Authentication
    - notes:
      + we use this to test auth (which is already 'tested' via our 'get_auth_token' above
          but we want a true test of auth plus we want to authenticate as the tester4update
          user in case the app side of this account is not yet set up.
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=test_auth_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
    assert response.status_code == 200
    assert response.json()["token_type"] == "bearer"


async def profiles_get_all_empty_test(get_auth_token):
    """
    Profile: get all
    - notes
      + should be none as we've not added any yet
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/profiles/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 0


async def profiles_add_test(get_auth_token):
    """
    Profile: add
    NOTE on 'params' they need to be encoded but you cannot do jsonable_encode() or json.dumps()
      on the entire object, you must do it at the individual field value!
    """

    global test_profile_id, test_profile_access_key
    test_profile["profile_id"] = test_profile_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/profiles/",
            params=test_profile,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    assert len(response.json()["access_key"]) > 0
    # set test_profile_id so we can use it to cleanup later.
    test_profile_id = response.json()["id"]
    test_profile_access_key = response.json()["access_key"]


async def profiles_get_all_test(get_auth_token):
    """
    Profile: get all
    - notes
      + should now be 1
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/profiles/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 1


async def profiles_update_test(get_auth_token):
    """
    Profile: update
    - notes
      + 'params' need to be encoded but you cannot do jsonable_encode() or json.dumps()
        on the entire object, you must do it at the individual field value!
    """

    global test_profile_id, test_profile
    test_profile["_id"] = test_profile_id
    test_profile["name"] = f"{test_profile['name']} Updated"
    test_profile["note"] = f"{test_profile['note']} Updated"
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/profiles/",
            params=test_profile,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


async def profiles_get_one_test(get_auth_token):
    """
    Profile: get a specific profile
    - notes
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global test_profile_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/profiles/one/{test_profile_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["profile_type"] == test_profile["profile_type"]
    assert response.json()["name"] == test_profile["name"]
    assert response.json()["note"] == test_profile["note"]
    assert response.json()["enabled"] == test_profile["enabled"]


async def dispatchers_get_all_test(get_auth_token):
    """
    Dispatcher: get all
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/dispatchers/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) > 0


async def dispatchers_get_all_limit_test(get_auth_token):
    """
    Dispatcher: get all, limiting to 2; this tests the limit functionality.
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/dispatchers/all",
            params={"limit": 2},
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 2


async def dispatchers_add_db_test(get_auth_token):
    """
    Dispatcher: add the db dispatcher
    """

    global test_profile_id, test_dispatcher_db_id
    test_dispatcher_db["profile_id"] = test_profile_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/dispatchers/",
            params=test_dispatcher_db,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set test_dispatcher_db_id so we can use it to cleanup later.
    test_dispatcher_db_id = response.json()["id"]


async def dispatchers_add_email_test(get_auth_token):
    """
    Dispatcher: add the email dispatcher
    """

    global test_profile_id, test_dispatcher_email_id
    test_dispatcher_email["profile_id"] = test_profile_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/dispatchers/",
            params=test_dispatcher_email,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set test_dispatcher_email_id so we can use it to cleanup later.
    test_dispatcher_email_id = response.json()["id"]


async def dispatchers_get_all_for_profile_test(get_auth_token):
    """
    Dispatcher: get all for profile
    - expect: two
    """

    global test_profile_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/dispatchers/all/{test_profile_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 2


async def dispatchers_update_test(get_auth_token):
    """
    Dispatcher: update
    - notes
      + 'params' need to be encoded but you cannot do jsonable_encode() or json.dumps()
        on the entire object, you must do it at the individual field value!
    """

    global test_dispatcher_db_id, test_dispatcher_db

    test_dispatcher_db["_id"] = test_dispatcher_db_id
    test_dispatcher_db["name"] = test_dispatcher_db["name"] + " Updated"
    test_dispatcher_db["note"] = test_dispatcher_db["note"] + " Updated"
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/dispatchers/",
            params=test_dispatcher_db,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


async def dispatchers_get_one_test(get_auth_token):
    """
    Dispatcher: get one
    - notes
      + this needs to be after test_dispatchers_update so we can check that its data was persisted.
    """

    global test_dispatcher_db_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/dispatchers/one/{test_dispatcher_db_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["id_"] == test_dispatcher_db_id
    assert response.json()["name"] == test_dispatcher_db["name"]
    assert response.json()["note"] == test_dispatcher_db["note"]
    assert response.json()["enabled"] == test_dispatcher_db["enabled"]


async def posts_form_test(get_auth_token):
    """
    Posts: Form
    - expect: successful post
    """

    global test_profile_access_key
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/posts/form/{test_profile_access_key}",
            data=test_post,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_form,
        )

    assert response.status_code == 200
    assert response.json()["success"] is True
    assert response.json()["message"] == "message received"


async def posts_count_test(get_auth_token):
    """
    Posts: count for profile
    - notes:
      + this test has a _cleanup_pause second sleep before execution to allow the
        test_posts_form to be dispatched (via worker)
    """

    global test_profile_id
    await asyncio.sleep(_cleanup_pause)
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/posts/count/{test_profile_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["success"] is True
    assert response.json()["count"] == 1


async def messages_get_all_test(get_auth_token):
    """
    Messages: get all
    - expect: one message (added earlier)
    - notes
      + this sets the test_message_id used by subsequent tests.
    """

    global test_message_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/messages/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 1
    test_message_id = response.json()[0]["id_"]
    assert test_message_id is not None


async def messages_get_all_by_access_key_test(get_auth_token):
    """
    Messages: get all by access key
    - expect: one message (added earlier)
    """

    global test_profile_access_key
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/messages/all/{test_profile_access_key}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) == 1


async def messages_get_one_test(get_auth_token):
    """
    Messages: get one
    """

    global test_message_id
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/messages/one/{test_message_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    _message = response.json()
    # NOTE: we only check the items which can be updated.
    assert _message["profile_id"] == test_profile_id
    assert _message["profile_type"] == "contact"
    assert _message["status"] == "new"
    assert _message["note"] is None
    assert _message["creator"] == "worker"
    _message_msg = _message["message"]
    del _message_msg["source_id"]  # source_id is not in post so strip it off
    assert _message_msg == test_post


async def users_me_test(get_auth_token):
    """
    User Me: get 'me' info for logged in user
    - notes
      + this is as the 'test_app_user' user
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/me",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["username"] == test_app_user["username"]


async def users_get_all_test(get_auth_token):
    """
    User: get all

    @TODO: this should likely be changed to a comprehension to see if drad and tester
           is in the results rather than drad as first response.
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()[0]["username"] == "drad"


async def users_update_test(get_auth_token):
    """
    User: update
    - notes
      + 'params' need to be encoded but you cannot do jsonable_encode() or json.dumps()
        on the entire object, you must do it at the individual field value!
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/users/{test_app_user_for_update_username}",
            params=test_app_user_for_update,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["ok"] is True


async def users_get_one_test(get_auth_token):
    """
    User: get a specific user
    - notes:
      + we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/{test_app_user_for_update_username}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["display_name"] == test_app_user_for_update["display_name"]
    assert response.json()["avatar"] == test_app_user_for_update["avatar"]
    assert response.json()["enabled"] == test_app_user_for_update["enabled"]
    assert response.json()["note"] == test_app_user_for_update["note"]


# ─────────────────────────────────────────────────────────────────────────────
# Miscellaneous (No order)
# NOTE: this comes before cleanup to give more time for above to complete.
# ─────────────────────────────────────────────────────────────────────────────
def status_test():
    """
    Status
    """

    response = client.get(f"{PATH_BASE}/status")
    assert response.status_code == 200
    assert response.json()["status"] == "OK"


def version_test():
    """
    Version
    """

    response = client.get(f"{PATH_BASE}/version")
    assert response.status_code == 200
    assert response.json() == app_version


def index_test():
    """
    Index (home) page
    """

    response = client.get(f"{PATH_SERVER}/")
    assert response.status_code == 200
    # a basic test if the response has the flow image.
    assert "fbkr-flow.png" in response.text


# ─────────────────────────────────────────────────────────────────────────────
# CLEANUP
# ─────────────────────────────────────────────────────────────────────────────


async def messages_delete_test(get_auth_token):
    """
    Message: delete
    """

    # ~ global test_dispatcher_db_id
    params = {"_id": test_message_id}
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/messages/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


async def dispatchers_delete_db_test(get_auth_token):
    """
    Dispatcher: delete db
    """

    global test_dispatcher_db_id
    params = {"_id": test_dispatcher_db_id}
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/dispatchers/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


async def dispatchers_delete_email_test(get_auth_token):
    """
    Dispatcher: delete email
    """

    global test_dispatcher_email_id
    params = {"_id": test_dispatcher_email_id}
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/dispatchers/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


async def profiles_delete_test(get_auth_token):
    """
    Profile: delete
    """

    global test_profile_id
    params = {"_id": test_profile_id}
    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/profiles/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


async def users_delete_test(get_auth_token):
    """
    User: delete
    - notes
      + we delete the 'test_app_user_for_update_username' account as it is 'recreated'
        on first login from LDAP.
    """

    async with AsyncClient(app=app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/users/{test_app_user_for_update_username}",
            headers={"Authorization": f"Bearer {get_auth_token}"},
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["id"] == test_app_user_for_update_username
    assert response.json()["msg"] == "deleted"
