#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import asyncio
import logging

from logs.utils import add_log_message


class DBHandler(logging.Handler):
    """
    Logging handler for couchdb.
    """

    def emit(self, record):
        """
        Emit the log message.
        """

        # print(f"CDB-LH> {record.name}, {record.levelname}, {record.module}, {record.funcName}, {record.lineno}, {record.process}, {record.msg}, {str(record.args)}")

        # write the log message to the Logs table.
        #   NOTE: we fire and forget as other handles (console, graylog) will also catch log message.
        asyncio.create_task(
            add_log_message(
                name=record.name,
                level=record.levelname,
                module=record.module,
                func_name=record.funcName,
                line_no=record.lineno,
                thread=record.thread,
                thread_name=record.threadName,
                process=record.process,
                message=record.msg,
                args=str(record.args),
            )
        )
