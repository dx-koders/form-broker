#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from typing import List, Union

import aiocouch
from config.config import ROUTE_AUTH, couchdb
from config.models import Cdb

logger = logging.getLogger("default")


async def check_databases():
    """
    Check to ensure needed databases exist, if not create them.
    """
    logger.info("Databases check:")
    for db in Cdb:
        try:
            # attempt connect - throws NotFoundError.
            await couchdb[db.value]
            logger.info(f"  ✓ {db.value}")
        except aiocouch.NotFoundError:
            logger.info(f"  ✦ {db.value} (missing but we created it!)")
            await couchdb.create(db.value)
    logger.info("Databases check all good!")


def codes_to_list(codes: Union[str, List] = None):
    """
    Convert string of codes to list of codes - supporting ranges
    """

    if isinstance(codes, List):
        return codes
    else:
        r = []
        for i in codes.split(","):
            if "-" not in i:
                r.append(int(i))
            else:
                _l, _h = map(int, i.split("-"))
                r += range(_l, _h + 1)
        return r


def fix_id(obj):
    if obj and obj.get("_id", False):
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        # ~ raise ValueError(f"No `_id` found! Unable to fix ID for: {obj}")
        return None


def get_route_roles(frame):
    """
    Get a route role
    Note: this uses the inspect.currentframe() to get the package (e.g. notifiers)
      and the method (e.g. get_by_username) which are used to get the appropriate
      route role from configs. Route roles in configs must be in form of
      package.method (e.g. notifiers.get_by_username).
    """

    from auth.models import Role

    try:
        auth = ROUTE_AUTH.get(frame.f_globals["__package__"]).get(frame.f_code.co_name)
        roles = []
        for r in auth:
            roles.append(Role(r))

        return roles
    except ValueError as e:
        logger.error(
            f"Looks like the package / method is not set up in api.route_auth (in config.toml)\n Error Message: {e}"
        )
        return None


def make_id(items: list = []):
    """
    Make id given the items supplied
        - concatenate all items with a : (e.g. item1:item2:item3
        - replace all spaces with - (dashes) (e.g. item 1 --> item-1)
        - transform entire string to lower case (e.g. Item1 --> item1)
    @return: id (as string)
    """

    _id = ":".join(items)

    return _id.lower().replace(" ", "-")


async def is_user(username: str) -> bool:
    """
    Check if user is a valid application user.
    """

    from users.utils import get_by_username

    # @TODO: this should likely get list of users out of redis to improve performance but should check.
    user = await get_by_username(username)
    if user:
        return True
    else:
        return False
