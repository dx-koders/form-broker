#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

from datetime import datetime
from typing import Optional, Union

from pydantic import BaseModel


class RunInterval(BaseModel):
    """
    Run Interval
    """

    weeks: int = 0
    days: int = 0
    hours: int = 0
    minutes: int = 0
    seconds: int = 0
    start_date: Union[datetime, str] = None
    end_date: Union[datetime, str] = None
    timezone: str = None


class DocInfo(BaseModel):
    """
    Document Info
    """

    ok: bool = None
    id: str = None
    rev: Optional[str] = None
    msg: Optional[str] = None
