#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.LOGS


class LogStatus(str, Enum):
    new = "new"
    addressed = "addressed"
    inconsequential = "inconsequential"
    watch = "watch"


class LogLevel(str, Enum):
    error = "error"
    warning = "warning"
    info = "info"


class LogBase(BaseModel):
    """the base"""

    name: str = (None,)  # the name
    level: str = (None,)  # the log level
    module: str = (None,)  # the module
    func_name: str = (None,)  # the function name
    line_no: str = (None,)  # the line number
    thread: str = (None,)  # the thread
    thread_name: str = (None,)  # the thread name
    process: str = (None,)  # the process
    message: str = (None,)  # the log message
    args: str = (None,)  # log args


class LogExt(LogBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Log(LogExt):
    """Actual (at DB level)"""

    id_: str


class LogResponse(Log):
    """Log Response"""

    pass
