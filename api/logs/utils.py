#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
import uuid
from datetime import datetime
from typing import Optional

import aiocouch
from _common.utils import make_id
from config.config import couchdb
from fastapi.encoders import jsonable_encoder
from logs.models import LogExt, _db

logger = logging.getLogger("default")


async def add_log_message(
    name: str = None,
    level: str = None,
    module: str = None,
    func_name: str = None,
    line_no: str = None,
    thread: str = None,
    thread_name: str = None,
    process: str = None,
    message: str = None,
    args: str = None,
    creator: Optional[str] = None,
):
    """
    Add a log message.
    """

    db = await couchdb[_db.value]

    log = LogExt(
        name=name,
        level=level,
        module=module,
        func_name=func_name,
        line_no=line_no,
        thread=thread,
        thread_name=thread_name,
        process=process,
        message=message,
        args=args,
        created=datetime.now(),
        updated=datetime.now(),
        creator=creator,
    )

    # ~ logger.critical(
    # ~ f"Log Message Received at {log.created}: level={level}, status={status}, location={location}, message={message}"
    # ~ )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(log),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
