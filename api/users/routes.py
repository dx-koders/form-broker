#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, Depends, HTTPException, Query, Security
from fastapi.encoders import jsonable_encoder
from pydantic import HttpUrl
from users.models import (
    User,
    UserBase,
    _db,
    get_current_active_user,
    get_current_user,
)

logger = logging.getLogger("default")
users_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.get("/me", response_model=UserBase)
async def me(current_user: UserBase = Depends(get_current_active_user)):
    """
    Get current user info.
    """

    return current_user


@users_router.get("/", response_model=List[User])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # @TODO: add limit and skip logic.
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.docs():
        _doc = fix_id(doc)
        _d = User(**_doc)
        docs.append(_d)

    return docs


@users_router.get("/{username}", response_model=User)
async def get_one(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """Get by username"""

    db = await couchdb[_db.value]

    _doc = fix_id(await _get_or_404(username, db))
    _d = User(**_doc)

    return _d


# 2020-07-01:drad update of user is limited to updating the following as other items are set via ldap:
#   - disabled: this will disable the user in the app
#   - note: an app specific note for the user
#   - accounts: a list of accounts the user belongs to
@users_router.put(
    "/{username}",
    response_model=dict,
)
async def update(
    username: str,
    enabled: bool = Query(True, description="Is the user enabled?"),
    display_name: str = Query(None, description="name used in application display"),
    avatar: HttpUrl = Query(None, description="user's avatar"),
    note: str = Query(None, description="note on user"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    """

    logger.debug(
        f"RECEIVED: username={username}, enabled={enabled}, display_name={display_name}, avatar={avatar}, note={note}"
    )
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    # @TODO currently dont allow update of account and name for now (need to update pk)
    doc["enabled"] = enabled
    doc["note"] = note if note else doc["note"]
    doc["display_name"] = display_name if display_name else doc["display_name"]
    doc["avatar"] = jsonable_encoder(avatar) if avatar else doc["avatar"]
    await doc.save()
    _doc = await doc.info()
    return _doc


@users_router.delete("/{username}", response_model=DocInfo)
async def delete(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    resp = DocInfo(ok=True, id=username, msg="deleted")
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
