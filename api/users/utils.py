#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime

import aiocouch
from _common.utils import make_id
from config.config import couchdb
from config.models import Cdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder

logger = logging.getLogger("default")


async def add_user(user=None):
    """
    Add user
    """

    db = await couchdb[Cdb.USERS.value]

    user.created = datetime.now()
    user.updated = datetime.now()

    try:
        doc = await db.create(make_id(items=[user.username]), data=jsonable_encoder(user))
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def get_by_username(username):
    """
    Get user by username
    """

    db = await couchdb[Cdb.USERS.value]

    try:
        return await db[username]
    except aiocouch.exception.NotFoundError:
        # NOTICE: we cannot raise exception here as downstream (ldap auth check) needs to account for no user found (so it can create).
        return None


async def update_user(user=None):
    """
    Update user
    """

    logger.debug(f"- updating user with: {user}")
    user.updated = datetime.now()

    try:
        doc = await get_by_username(user.username)
        doc["enabled"] = user.enabled
        doc["note"] = user.note
        doc["first_name"] = user.first_name
        doc["last_name"] = user.last_name
        doc["display_name"] = user.display_name
        doc["email"] = user.email
        doc["avatar"] = user.avatar
        await doc.save()

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def verify_user(user=None):
    """
    Verify given user exists.
    Returns the user if found else Raise 404.
    """

    if user:
        return await get_by_username(user)
    else:
        raise HTTPException(status_code=404, detail="Not found")


async def check_preference(user=None, preference=None, value=None):
    """
    Check if user has a given preference.
    check_preference(user='drad', preference='process_activity_notes', True)
    """

    logger.debug(f"- check_preference on user=[{user}], preference=[{preference}], value=[{value}]")

    if user and preference and value:
        _user = await get_by_username(user)
        if not _user:
            logger.error(f"ERROR: check_preferences user [{user}] not found!")
            return False
        elif "preferences" not in _user:
            logger.error("User has no preferences defined")
            return False
        elif preference not in _user["preferences"]:
            logger.error(f"User preferences does not have needed preference defined: {preference}")
            return False
        elif not _user["preferences"][preference] == value:
            logger.debug(f"User preference of '{preference}' does not match expected value")
            return False
        else:
            logger.debug(f"User preferences of '{preference}' matches expected value")
            return True
    else:
        logger.error("User preference and value not supplied")
        return False
