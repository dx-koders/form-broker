#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime, timedelta
from typing import Optional

import jwt
from config import config
from config.config import cfg
from users.models import UserBase

logger = logging.getLogger("default")


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        config.API_JWT_SECRET_KEY,
        algorithm=cfg.api.jwt.algorithm,
    )
    return encoded_jwt


# ~ async def has_account_access(
# ~ current_user: UserBase = None,
# ~ account: Account = None,
# ~ roles: Role = None,
# ~ ) -> bool:
# ~ """
# ~ Check if user has account access
# ~ """

# ~ # get role for account in question.
# ~ # logger.debug(f"- current user accounts: {current_user.accounts}")
# ~ # logger.debug(f"- account: {account}")
# ~ ac_role = [a.role for a in current_user.accounts if a.id_ == account.id_][0]
# ~ # return if role for account is in role(s) list.
# ~ return ac_role in roles


async def has_role(user: UserBase = None, role: str = None) -> bool:
    """
    Check if user has a given role
    """

    return role in user.roles
