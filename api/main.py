#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>
#
# LOGGING: designed to run at INFO loglevel.

import logging
from os import getenv

from _common.log_handlers import DBHandler
from _common.utils import check_databases
from auth.routes import auth_router
from config import config
from config.config import cfg
from core.routes import core_router
from dispatchers.routes import dispatchers_router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from messages.routes import messages_router
from posts.routes import posts_router
from profiles.routes import profiles_router
from users.routes import users_router
from contextlib import asynccontextmanager

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(config.APP_LOGLEVEL))
console_handler = logging.StreamHandler()
db_handler = DBHandler()
db_handler.setLevel(logging.getLevelName(config.APP_DB_LOGLEVEL))

if "console" in config.LOG_TO:
    logger_base.addHandler(console_handler)
if "db" in config.LOG_TO:
    logger_base.addHandler(db_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": cfg.core.name, "application_env": config.DEPLOY_ENV},
)

logger.info(f"{cfg.core.name} - v.{cfg.core.version} ({cfg.core.modified}) - {config.APP_LOGLEVEL} - {config.LOG_TO}")

# log startup items
_lsi = []
_lsi.append(("Log Level", config.APP_LOGLEVEL))
_lsi.append(("Log To", config.LOG_TO))
_lsi.append(("Deploy Env", config.DEPLOY_ENV))
_lsi.append(("CDB URI", config.CDB_URI))
_lsi.append(("API Path", config.API_PATH))
_lsi.append(("Auth URL", f"{config.LDAP_HOST}:{config.LDAP_PORT}"))
_lsi.append(("CORS Origins", config.CORS_ORIGINS))
_lsi.append(
    (
        "JWT Expire",
        f"{config.API_JWT_EXPIRE_DAYS}:{config.API_JWT_EXPIRE_HOURS}:{config.API_JWT_EXPIRE_MINUTES} (Days:Hours:Minutes)",
    )
)
_lsi.append(
    (
        "LDAP",
        f"{config.LDAP_HOST}:{config.LDAP_PORT} {'(using ssl)' if config.LDAP_USE_SSL else ''}",
    )
)
_lsi.append(
    (
        "Redis",
        f"{getenv('REDIS_HOST', 'undefined')}:{getenv('REDIS_PORT', 'undefined')}/{getenv('REDIS_DB', 'undefined')} (type={getenv('REDIS_TYPE', 'undefined')}){' - with password' if getenv('REDIS_PASSWORD') else ' - NO PASSWORD'}",
    )
)

# find longest key length and add 4
lsi_lk_length = len(sorted(_lsi, key=lambda x: len(x[0]), reverse=True)[0][0]) + 4
# sort items by key
_lsi.sort(key=lambda a: a[0])
for item in _lsi:
    logger.info(f"  ✦ {item[0].ljust(lsi_lk_length, '.')}: {item[1]}")


@asynccontextmanager
async def lifespan(app: FastAPI):
    # ensure db is setup properly.
    await config.couchdb.check_credentials()
    await check_databases()
    yield
    # close db connection.
    await config.couchdb.close()


app = FastAPI(
    lifespan=lifespan,
    title=f"{cfg.core.name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{config.API_PATH}/openapi.json",
    docs_url=f"{config.API_PATH}/docs",
    redoc_url=None,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=config.CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    dispatchers_router,
    prefix=f"{config.API_PATH}/dispatchers",
    tags=["dispatchers"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    messages_router,
    prefix=f"{config.API_PATH}/messages",
    tags=["messages"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    posts_router,
    prefix=f"{config.API_PATH}/posts",
    tags=["posts"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    profiles_router,
    prefix=f"{config.API_PATH}/profiles",
    tags=["profiles"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{config.API_PATH}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    auth_router,
    prefix=f"{config.API_PATH}/auth",
    tags=["authentication"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{config.API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)

# NOTE: this is the mount to service html/css files for the index (home) page.
app.mount("/", StaticFiles(directory="_static", html=True), name="static")
