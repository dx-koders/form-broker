#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from profiles.models import ProfileType
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.MESSAGES


class MessageStatus(str, Enum):
    new = "new"
    read = "read"
    deleted = "deleted"


class MessageData(BaseModel):
    name: str = None
    email: str = None
    subject: str = None
    message: str = None
    source_id: Optional[str] = None


class MessageBase(BaseModel):
    """
    the base
    """

    profile_id: str = None  # profile the message belongs to
    profile_type: ProfileType = None  # profile type
    status: MessageStatus = None  # the status of the item
    # message: Json = None  # the message
    # NOTE: using a Json object for message has an issue in fastapi/pydantic response processing
    #  where it throws an error in validating the response, for now we are going with a class datatype
    #  as it works and I do not think it breaks anything.
    message: MessageData = None  # the message
    note: Optional[str] = None  # notes for the message


class MessageExt(MessageBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Message(MessageExt):
    """
    Actual (at DB level)
    """

    id_: str


class MessageResponse(Message):
    """
    Message Response
    """

    pass
