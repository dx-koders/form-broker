#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
import json
import uuid
from datetime import datetime

import aiocouch
from _common.utils import make_id
from config.config import couchdb
from fastapi.encoders import jsonable_encoder
from messages.models import MessageExt, MessageData, MessageStatus, _db

logger = logging.getLogger("default")


async def add_message(
    profile_id: str = None,
    profile_type: str = None,
    message: str = None,
    creator: str = None,
):
    """
    Add a message.
    """

    db = await couchdb[_db.value]
    _message_data = MessageData(**json.loads(message))
    # ~ logger.debug(f"- adding Message; profile_id={profile_id}, profile_type={profile_type}, creator={creator}, message={_message_data}")
    _doc = MessageExt(
        profile_id=profile_id,
        profile_type=profile_type,
        status=MessageStatus.new,
        message=_message_data,
        created=datetime.now(),
        updated=datetime.now(),
        creator=creator,
    )

    # ~ logger.debug(f"- saving Message of={_doc}")

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
