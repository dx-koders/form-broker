#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from messages.models import MessageResponse, _db
from profiles.utils import get_profile_by_access_key
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
messages_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@messages_router.get("/all", response_model=List[MessageResponse])
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all logs
    - only for admins
    """

    logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        _doc = fix_id(doc)
        _d = MessageResponse(**_doc)
        docs.append(_d)

    return docs


@messages_router.get("/all/{access_key}", response_model=List[MessageResponse])
async def get_all_for_profile(
    access_key: str,
    limit: int = Query(10, ge=1, le=99, description="limit number of results"),
    skip: int = Query(0, description="skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all Messages for given profile (by access_key).
    """

    logger.debug(f"- get all for profile: {access_key}")
    db = await couchdb[_db.value]

    # ensure current_user is allowed for profile.
    _profile = await get_profile_by_access_key(access_key=access_key)
    if not _profile or not _profile["owner"] == current_user.username:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not enough permissions")

    selector = {"profile_id": _profile["_id"]}
    logger.debug(f"- get_all selector: {selector}")
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        _doc = fix_id(doc)
        _d = MessageResponse(**_doc)
        docs.append(_d)

    return docs


@messages_router.get("/one/{_id}", response_model=MessageResponse)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get one by id
    - only for admins
    """

    db = await couchdb[_db.value]
    _doc = fix_id(await _get_or_404(_id, db))
    _d = MessageResponse(**_doc)

    return _d


@messages_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    - only for admins
    """

    db = await couchdb[_db.value]

    # Delete the dispatcher
    resp = DocInfo(ok=True, id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
