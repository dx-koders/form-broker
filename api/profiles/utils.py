#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging

from config.config import couchdb
from profiles.models import _db

logger = logging.getLogger("default")


async def get_profile_by_access_key(access_key: str = None):
    """
    Get profile by access_key.
    NOTE: this will return the profile if found, otherwise None is returned.
    """

    logger.debug("- get profile by access key...")
    db = await couchdb[_db.value]

    selector = {"access_key": access_key, "enabled": True}

    logger.debug(f"- selector: {selector}")
    docs = []
    async for doc in db.find(selector=selector):
        docs.append(doc)

    if len(docs) == 1:
        # return the only doc
        return docs[0]
    elif len(docs) > 1:
        # need to log an error here as this should not happen
        return None
    else:
        return None
