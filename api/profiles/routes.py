#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
import uuid
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from profiles.models import (
    ProfileAddResponse,
    ProfileExt,
    ProfileResponse,
    ProfileType,
    _db,
)
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
profiles_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@profiles_router.get("/all", response_model=List[ProfileResponse])
async def get_all(
    # ~ owner: str = Query(
    # ~ None, description="The doer of the activity (only applicable for admin users)"
    # ~ ),
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for user
    - doer is only applicable for users who are admin
    """

    logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    selector = {}
    # allow overriding if admin
    # ~ if await has_role(current_user, "admins"):
    # ~ if doer:
    # ~ # add doer filter (otherwise admin will get all)
    # ~ selector["doer"] = doer
    # ~ else:
    # ~ selector["doer"] = doer
    selector["owner"] = current_user.username

    logger.debug(f"- get_all selector: {selector}")
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        _doc = fix_id(doc)
        _d = ProfileResponse(**_doc)
        docs.append(_d)

    return docs


@profiles_router.get("/one/{_id}", response_model=ProfileResponse)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await couchdb[_db.value]
    _doc = fix_id(await _get_or_404(_id, db))
    _d = ProfileResponse(**_doc)

    return _d


@profiles_router.post("/", response_model=ProfileAddResponse, status_code=status.HTTP_201_CREATED)
async def add(
    profile_type: ProfileType = Query(ProfileType.contact, description="profile type"),
    name: str = Query(..., description="name of the profile"),
    note: str = Query(None, description="notes for the profile"),
    enabled: bool = Query(True, description="is profile enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add Profile
    NOTE: an access key will be generated on create and returned in the response message.
    """

    db = await couchdb[_db.value]

    logger.debug("- add Profile")

    # generate an access key on all Adds.
    access_key = str(uuid.uuid4())

    profile = ProfileExt(
        profile_type=profile_type,
        name=name,
        note=note,
        access_key=access_key,
        enabled=enabled,
        owner=current_user.username,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(profile),
        )
        await doc.save()
        ret = {}
        ret["access_key"] = access_key
        ret.update(await doc.info())

        logger.debug(f"- ret is: {ret}")

        _d = ProfileAddResponse(**ret)
        logger.debug(f"- Returning: {_d}")

        return _d

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@profiles_router.put("/", response_model=ProfileAddResponse)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    profile_type: ProfileType = Query(ProfileType.contact, description="profile type"),
    name: str = Query(None, description="name of the profile"),
    note: str = Query(None, description="notes for the profile"),
    enabled: bool = Query(True, description="is profile enabled?"),
    generate_new_access_key: bool = Query(False, description="generate a new access key?"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update Profile
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")
    logger.debug(f"- profile_type: {profile_type}")

    doc["profile_type"] = profile_type if profile_type else doc["profile_type"]
    doc["name"] = name if name else doc["name"]
    doc["note"] = note if note else doc["note"]
    doc["enabled"] = enabled

    if generate_new_access_key:
        doc["access_key"] = str(uuid.uuid4())

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()

    ret = {}
    ret["access_key"] = doc["access_key"]
    ret.update(await doc.info())

    return ret


@profiles_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    db = await couchdb[_db.value]

    # Delete the profile
    resp = DocInfo(ok=True, id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
