#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from _common.models import DocInfo
from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.PROFILES


class ProfileType(str, Enum):
    contact = "contact"


class ProfileBase(BaseModel):
    """
    Base
    """

    owner: str = None  # user who owns the profile
    profile_type: ProfileType = None  # profile type
    name: str = None  # name of the profile
    access_key: str = None  # access key for profile

    note: Optional[str] = None  # notes about profile
    enabled: bool = True  # is profile enabled?


class ProfileExt(ProfileBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Profile(ProfileExt):
    """
    Actual (at DB level)
    """

    id_: str


class ProfileResponse(Profile):
    """
    Response
    """

    pass


class ProfileAddResponse(DocInfo):
    """
    Add Response
    """

    access_key: str = None
