#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import asyncio
import json
import logging

from aiohttp import ClientSession
from arq import create_pool
from arq.connections import RedisSettings
from config.config import (
    APP_LOGLEVEL,
    DEPLOY_ENV,
    LOG_TO,
    REDIS_DB_ARQ,
    REDIS_HOST,
    REDIS_PASSWORD,
    REDIS_PORT,
    cfg,
)
from dispatchers.models import DispatcherMessage, DispatcherType
from dispatchers.utils import (
    email_sender,
    get_dispatchers_for_profile,
)
from profiles.models import ProfileBase
from messages.utils import add_message
from posts.utils import audit_post
from profiles.utils import get_profile_by_access_key

redis = None  # will contain the redis connection pool.
app_name = f"{cfg.core.name}-worker"

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(APP_LOGLEVEL))
console_handler = logging.StreamHandler()
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": app_name, "application_env": DEPLOY_ENV},
)

if APP_LOGLEVEL.upper() == "DEBUG":
    logger.info("### App Started With Debug On ###")

logger.info(f"{app_name} - v.{cfg.core.version} ({cfg.core.modified}) - {APP_LOGLEVEL} - {LOG_TO}")
logger.info(f"Worker DB:    {REDIS_HOST}:{REDIS_PORT}/{REDIS_DB_ARQ}")


async def coordinator(ctx, access_key, data):
    """
    Coordinator handles the processing of all data submitted.
    """

    logger.info(f"received task with access_key={access_key} and data={data}")

    # check access key
    _profile = await get_profile_by_access_key(access_key)
    if _profile:
        # audit post by saving the date/time, profile, profile type, source ip
        _audit = await audit_post(
            source_ip=data["source_ip"],
            profile_id=_profile["_id"],
            profile_type=_profile["profile_type"],
        )
        logger.debug(f"post audited: {_audit}")

        # @TODO: [idea] we could implement blacklisting here (if ip is in known spam list dont process it)
        # @TODO: [performance] currently items are synchronous within worker as there is not much reason to separate, may want to put items back on arq for delivery for scalability later.
        if _profile:
            logger.info(f"valid access key found, associated profile_id={_profile['_id']}")

            # get dispatchers.
            _dispatchers = await get_dispatchers_for_profile(profile_id=_profile["_id"])
            if _dispatchers:
                for dispatcher in _dispatchers:
                    if dispatcher["dispatcher_type"] == DispatcherType.database:
                        logger.debug("sending to persistence ('Messages' table)")
                        # write to Messages table
                        await add_message(
                            profile_id=_profile["_id"],
                            profile_type=_profile["profile_type"],
                            message=json.dumps(data),
                            creator="worker",
                        )
                    elif dispatcher["dispatcher_type"] == DispatcherType.email:
                        logger.debug(f"send email via: {dispatcher['name']}")
                        _dispatcher_message = DispatcherMessage(
                            config=json.dumps(dispatcher["config"].decode()),
                            data=data,
                            profile=ProfileBase(**_profile),
                        )
                        await email_sender(dispatcher_message=_dispatcher_message)

            else:
                logger.warning(f"No enabled dispatchers found for profile id: {_profile['_id']}")

        else:
            logger.warning("INVALID ACCESS KEY, message will be silently skipped")
    else:
        # NOTICE: we log this as it could be a phishing attempt and then stop processing.
        logger.warning(f"No Profile found for access key: {access_key} - cannot send data={data}")


async def startup(ctx):
    global redis

    ctx["session"] = ClientSession()

    redis = await create_pool(
        RedisSettings(
            host=REDIS_HOST,
            port=REDIS_PORT,
            database=REDIS_DB_ARQ,
            password=REDIS_PASSWORD,
        )
    )


async def shutdown(ctx):
    await ctx["session"].close()


# WorkerSettings defines the settings to use when creating the work, it's used by the arq cli
class WorkerSettings:
    # NOTE: container runs with UTC time.
    # ~ cron_jobs = [
    # ~ cron(
    # ~ activity_processor_process,
    # ~ second=13,
    # ~ ),
    # ~ cron(
    # ~ activity_processor_retry,
    # ~ second=33,
    # ~ ),
    # ~ ]
    # ~ functions = [activity_processor_process, verify_or_create_related_item_overview]
    functions = [coordinator]
    on_startup = startup
    on_shutdown = shutdown
    health_check_interval = 60

    redis_settings = RedisSettings(
        host=REDIS_HOST,
        port=REDIS_PORT,
        database=REDIS_DB_ARQ,
        password=REDIS_PASSWORD,
    )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
