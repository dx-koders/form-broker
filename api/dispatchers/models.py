#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from _common.models import DocInfo
from config.models import Cdb
from profiles.models import ProfileBase
from pydantic import BaseModel, Json

logger = logging.getLogger("default")
_db = Cdb.DISPATCHERS


class DispatcherType(str, Enum):
    email = "email"
    database = "database"
    # mattermost = "mattermost


class DispatcherBase(BaseModel):
    """
    Base
    """

    name: str = None  # name of the dispatcher
    dispatcher_type: DispatcherType = None  # dispatcher type
    config: Optional[str] = None  # the json config for a dispatcher as an encrypted base64 encoded bytes
    note: Optional[str] = None  # notes about profile
    enabled: bool = True  # is profile enabled?


class DispatcherExt(DispatcherBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Dispatcher(DispatcherExt):
    """
    Actual (at DB level)
    """

    id_: str


class DispatcherResponse(Dispatcher):
    """
    Response
    """

    # config: Json = None
    pass


class DispatcherAddResponse(DocInfo):
    """
    Add Response
    """

    pass


class DispatcherMessage(BaseModel):
    """
    A dispatcher message (generic structure used in sending notifications)
    """

    config: Json = None  # config of dispatcher
    data: dict = None  # data to send
    profile: ProfileBase = None  # profile the message belongs to
