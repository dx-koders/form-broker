#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import json
import logging
import uuid
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import make_id
from config.config import API_APP_KEY, couchdb
from cryptography.fernet import Fernet
from dispatchers.models import (
    DispatcherAddResponse,
    DispatcherExt,
    DispatcherResponse,
    DispatcherType,
    _db,
)
from dispatchers.utils import fix_doc
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from pydantic import Json
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
dispatchers_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@dispatchers_router.get("/all", response_model=List[DispatcherResponse])
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    - only for admins
    """

    logger.debug("- get all")
    db = await couchdb[_db.value]
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        # ~ logger.debug(f"- before fix: {doc}")
        _doc = await fix_doc(doc)
        # ~ logger.debug(f"- Fixed Doc is: {_doc}")
        # I think the issue is null config on some docs...need to investigate
        _d = DispatcherResponse(**_doc)
        # ~ logger.debug(f"- doc cast to DispatcherResponse: {_d}")
        docs.append(_d)

    return docs


@dispatchers_router.get("/all/{profile_id}", response_model=List[DispatcherResponse])
async def get_all_for_profile(
    profile_id: str,
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all Dispatchers for given profile.
    """

    logger.debug(f"- get all for profile_id: {profile_id}")
    db = await couchdb[_db.value]

    selector = {"_id": {"$regex": f"^{profile_id}:.*$"}}
    logger.debug(f"- get_all selector: {selector}")
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        _doc = await fix_doc(doc)
        _d = DispatcherResponse(**_doc)
        docs.append(_d)

    return docs


@dispatchers_router.get("/one/{_id}", response_model=DispatcherResponse)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await couchdb[_db.value]

    _doc = await fix_doc(await _get_or_404(_id, db))
    _d = DispatcherResponse(**_doc)

    return _d


@dispatchers_router.post("/", response_model=DispatcherAddResponse, status_code=status.HTTP_201_CREATED)
async def add(
    profile_id: str = Query(..., description="id of profile the dispatcher belongs to"),
    dispatcher_type: DispatcherType = Query(..., description="dispatcher type"),
    name: str = Query(..., description="name of the dispatcher"),
    config: Json = Query(None, description="dispatcher config as json if applicable"),
    note: str = Query(None, description="notes for the dispatcher"),
    enabled: bool = Query(True, description="is dispatcher enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add Dispatcher
    NOTE: an access key will be generated on create and returned in the response message.
    """

    db = await couchdb[_db.value]

    logger.debug("- add Dispatcher")
    fernet = Fernet(API_APP_KEY)

    dispatcher = DispatcherExt(
        dispatcher_type=dispatcher_type,
        name=name,
        config=fernet.encrypt(str.encode(json.dumps(config))),
        note=note,
        enabled=enabled,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[profile_id, str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(dispatcher),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@dispatchers_router.put("/", response_model=DispatcherAddResponse)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    dispatcher_type: DispatcherType = Query(..., description="dispatcher type"),
    name: str = Query(None, description="name of the dispatcher"),
    config: Json = Query(None, description="dispatcher config as json"),
    note: str = Query(None, description="notes for the dispatcher"),
    enabled: bool = Query(True, description="is dispatcher enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update Dispatcher
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")
    fernet = Fernet(API_APP_KEY)

    doc["dispatcher_type"] = dispatcher_type if dispatcher_type else doc["dispatcher_type"]
    doc["name"] = name if name else doc["name"]
    doc["config"] = jsonable_encoder(fernet.encrypt(str.encode(json.dumps(config)))) if config else doc["config"]
    doc["note"] = note if note else doc["note"]
    doc["enabled"] = enabled

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()

    return await doc.info()


@dispatchers_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Hard Delete by id
    """

    db = await couchdb[_db.value]

    # Delete the dispatcher
    resp = DocInfo(ok=True, id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
