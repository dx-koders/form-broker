#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import json
import logging

import markdown
from _common.utils import fix_id
from config.config import API_APP_KEY, DEPLOY_ENV, cfg, couchdb
from cryptography.fernet import Fernet
from dispatchers.models import DispatcherMessage, _db

logger = logging.getLogger("default")


async def fix_doc(doc):
    """
    Fix doc by fixing its id and properly encoding the config (json) for response.
    """

    _doc = fix_id(doc)
    logger.debug(f"- fix_id returned: {_doc}")
    # _doc["config"] = json.dumps(_doc["config"]) if "config" in doc else ""
    if "config" in _doc and doc["config"]:
        # ~ logger.debug("- have a config to handle...")
        fernet = Fernet(API_APP_KEY)
        # ~ logger.debug("- decrypting config...")
        _doc["config"] = fernet.decrypt(str.encode(_doc["config"]))
        # to get as json:
        # ~ _doc["config"] = json.dumps(json.loads(fernet.decrypt(str.encode(_doc["config"])).decode()))

        # ~ logger.debug(f"- decrypted config is: {_doc['config']}")

    return _doc


async def get_dispatchers_for_profile(profile_id: str = None):
    """
    Get dispatchers for profile.
    """

    logger.debug(f"- get dispatchers for profile id: {profile_id}")
    db = await couchdb[_db.value]

    selector = {
        "_id": {"$regex": f"^{profile_id}:.*$"},
        "enabled": True,
    }
    logger.debug(f"- selector: {selector}")
    docs = []
    async for doc in db.find(selector=selector):
        docs.append(await fix_doc(doc))

    return docs


async def email_sender(dispatcher_message: DispatcherMessage):
    """
    Send message via email
    """

    logger.info("sending via email")

    import smtplib
    import ssl
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    config = json.loads(dispatcher_message.config)

    msg = MIMEMultipart("alternative")
    msg["Subject"] = (
        f"{dispatcher_message.data['subject']} ({dispatcher_message.profile.name})"
        if DEPLOY_ENV == "prd"
        else f"{dispatcher_message.data['subject']} ({dispatcher_message.profile.name}) - {DEPLOY_ENV}"
    )

    # ~ logger.debug(f"─────> email send info; server_url={config['server_url']}, server_port={config['server_port']}, server_username={config['username']}, server_password={config['password']}, from={config['from']}, to={config['to']}, body=SKIPPED")

    msg["From"] = config["from"]
    msg["To"] = config["to"]
    # ~ part1 = MIMEText(f"{notifier_message.body}", "plain")
    # ~ msg_body = markdown.markdown(
    # ~ f"<style>table, th, td {{ border: 1px solid #7F7F7F; border-collapse: collapse; padding: 3 3 3 3; text-align: center;}} table {{ width: 70%; margin-left: 15%; margin-right: 15%; }} th {{ background-color: #7F7F7F; }} tr:nth-child(even) {{background-color: #f2f2f2;}}</style>{notifier_message.body}",
    # ~ extensions=["tables"],
    # ~ output_format="html5",
    # ~ )
    msg_body = markdown.markdown(
        f"""**{cfg.core.name}** Message - {dispatcher_message.profile.name}

From: {dispatcher_message.data['name']} <{dispatcher_message.data['email']}>

Message:

{dispatcher_message.data['message']}""",
        output_format="html5",
    )
    # ~ logger.critical(f"- email body formatted ({type(msg_body)}): {msg_body}")
    # ~ part1 = MIMEText(f"{markdowner.convert(notifier_message.body)}", "html")
    part1 = MIMEText(msg_body, "html")
    # part2 = MIMEText(text, 'html')
    msg.attach(part1)
    # msg.attach(part2)
    context = ssl.create_default_context()
    try:
        # included for mailhog testing as no ssl/no auth should generally not be used.
        if "no_ssl" in config and config["no_ssl"] and "no_auth" in config and config["no_auth"]:
            with smtplib.SMTP(config["server_url"], config["server_port"]) as server:
                # server.set_debuglevel(1)
                # server.login(sender_act, sender_pwd)
                # server.sendmail(sender_email, receiver_email, msg.as_string())
                server.send_message(msg)
            return {"success": True, "message": "Message sent"}
        else:
            with smtplib.SMTP_SSL(config["server_url"], config["server_port"], context=context) as server:
                server.login(
                    config["username"],
                    config["password"],
                )
                server.send_message(msg)
                server.quit()
            return {"success": True, "message": "Message sent"}

    except Exception as e:
        msg = f"Error sending message via email: {e}"
        logger.error(msg)
        return {"success": False, "message": msg}

    return True
