#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

from datetime import datetime

from config.config import cfg
from fastapi import APIRouter, Query, Request

core_router = APIRouter()


@core_router.get("/status", status_code=200)
def status():
    """[summary]
    Standard status

    [description]
    Return 200 for health checks.
    """
    return {"status": "OK", "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}


@core_router.get("/version", status_code=200)
def version():
    """[summary]
    Application version

    [description]
    Shows application version info
    """
    return {
        "status": "OK",
        "version": cfg.core.version,
        "created": cfg.core.created,
        "modified": cfg.core.modified,
    }


@core_router.get("/test/client-info", status_code=200)
def read_root(
    request: Request,
    access_key: str = Query(..., description="Profile access key"),
):
    """[summary]
    TEST ROUTE: Show client-info

    [description]
    Show client info.
    """
    r = {
        "client_host": request.client.host,
        "headers": request.headers,
    }
    return {**r}
