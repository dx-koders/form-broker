#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2024 drad <sa@adercon.com>

import logging
import tomllib
from os import getenv

import redis.asyncio as aioredis
from aiocouch import CouchDB
from config.models import ConfigBase

logger = logging.getLogger("default")

# redis/arq queue will be bound here on 'startup'
queue = None


def env_strtobool(val: str) -> bool:
    if val in ["true", "'true'"]:
        return True
    return False


def csv_to_set(in_str: str) -> set:
    return set(map(int, in_str.split(",")))


def load_config(cfg_file: str = None) -> dict:
    """
    Load config data from toml config file
    """

    conf = {}
    try:
        with open(cfg_file, "rb") as f:
            conf = tomllib.load(f)
    except FileNotFoundError as e:
        print(f"File not found: {e}")
    except Exception as e:
        print(f"Other Exception: {e}")
    return conf


async def get_sse_redis():
    """
    Get the SSE Redis instance.

    NOTE: this uses a connection pool
    NOTE: SSE uses redis db 1
    """

    return await aioredis.Redis(connection_pool=redis_sse_pool)


cfg = ConfigBase.parse_obj(load_config("config/config.toml"))

# key items
ROUTE_AUTH = cfg.api.route_auth

API_APP_KEY = getenv("API_APP_KEY", "")
API_BASE_PATH = getenv("API_BASE_PATH", "/api")
API_PATH = f"{API_BASE_PATH}/{cfg.api.versions.current}"

API_JWT_EXPIRE_MINUTES = int(getenv("API_JWT_EXPIRE_MINUTES", "0"))
API_JWT_EXPIRE_HOURS = int(getenv("API_JWT_EXPIRE_HOURS", "0"))
API_JWT_EXPIRE_DAYS = int(getenv("API_JWT_EXPIRE_DAYS", "1"))
API_JWT_SECRET_KEY = getenv("API_JWT_SECRET_KEY", None)

# ENV Based Variables
DEPLOY_ENV = getenv("DEPLOY_ENV", "prd")

# logging
LOG_TO = getenv("LOG_TO", "console").split(",")
APP_LOGLEVEL = getenv("LOG_LEVEL", "WARNING")
APP_DB_LOGLEVEL = getenv("DB_LOGLEVEL", "WARNING")

CORS_ORIGINS = getenv("CORS_ORIGINS", "http://localhost:8080").split(",")
CDB_URI = getenv("DB_CB_URI", "http://db:5984")
CDB_USER = getenv("DB_CB_USER", "admin")
CDB_PASS = getenv("DB_CB_PASS", "couchdb")

couchdb = CouchDB(CDB_URI, user=CDB_USER, password=CDB_PASS)

# LDAP
LDAP_HOST = getenv("LDAP_HOST", None)
LDAP_PORT = int(getenv("LDAP_PORT", "636"))
LDAP_USE_SSL = env_strtobool(getenv("LDAP_USE_SSL", "true"))
LDAP_USE_STARTTLS = env_strtobool(getenv("LDAP_USE_STARTTLS", "false"))
# The RDN attribute for your user schema on LDAP
LDAP_USER_RDN_ATTR = "cn"
# The Attribute you want users to authenticate to LDAP with.
LDAP_USER_LOGIN_ATTR = "cn"
# user attributes to get.
# LDAP_GET_USER_ATTRIBUTES = ldap3.ALL_ATTRIBUTES
# search with bind user.
# ~ LDAP_ALWAYS_SEARCH_BIND = True
LDAP_ALWAYS_SEARCH_BIND = False
# The Username to bind to LDAP with
LDAP_BIND_USER_DN = getenv("LDAP_BIND_USER_DN", None)
# The Password to bind to LDAP with
LDAP_BIND_USER_PASSWORD = getenv("LDAP_BIND_USER_PASSWORD", None)
# Base DN of your directory
LDAP_BASE_DN = getenv("LDAP_BASE_DN", None)
# Users DN to be prepended to the Base DN (ou=users)
# ~ LDAP_USER_DN = 'ou=users'
LDAP_USER_DN = ""
# Group settings
# NOTICE: keep the following to False; we do not use Flask-Ldap3-Login's group search as something dont work with it, rather we use ldap3 directly.
LDAP_SEARCH_FOR_GROUPS = False
# Groups DN to be prepended to the Base DN
# LDAP_GROUP_DN = 'ou=groups'
# LDAP_GROUP_OBJECT_FILTER = '(objectClass=groupOfNames)'
# LDAP_GROUP_OBJECT_FILTER = None
# LDAP_GROUP_MEMBERS_ATTR = 'member'
# Group scope ([LEVEL], SUBTREE) there are other levels (base/object, subordinates
# LDAP_GROUP_SEARCH_SCOPE = 'LEVEL'
LDAP_APP_GROUP_PREFIX = getenv("LDAP_APP_GROUP_PREFIX", None)
LDAP_USER_ROLE_SEARCH_BASE = getenv("LDAP_USER_ROLE_SEARCH_BASE", None)
LDAP_USER_ROLE_SEARCH_FILTER = getenv("LDAP_USER_ROLE_SEARCH_FILTER", None)
LDAP_USER_ROLE_ATTRIBUTES = getenv("LDAP_USER_ROLE_ATTRIBUTES", None)
LDAP_USER_ROLE_BATCH = env_strtobool(getenv("LDAP_USER_ROLE_BATCH", "true"))
LDAP_USER_CHECK_FOR_ENABLED = env_strtobool(getenv("LDAP_USER_CHECK_FOR_ENABLED", "true"))
LDAP_USER_SEARCH_BASE = getenv("LDAP_USER_SEARCH_BASE", None)
LDAP_USER_SEARCH_FILTER = getenv("LDAP_USER_SEARCH_FILTER", None)

REDIS_HOST = getenv("REDIS_HOST", "redis")
REDIS_PORT = int(getenv("REDIS_PORT", "6739"))
REDIS_PASSWORD = getenv("REDIS_PASSWORD", None)
REDIS_DB_ARQ = int(getenv("REDIS_DB_ARQ", "1"))

redis_sse_pool = aioredis.ConnectionPool.from_url(f"redis://{REDIS_HOST}:{REDIS_PORT}/1", password=REDIS_PASSWORD)
