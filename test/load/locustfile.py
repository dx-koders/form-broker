import os

from locust import HttpUser, TaskSet, between, task

# install: `pip install locustio`
# start server: `locust -f locustfile.py`
#  - to start with ssl verification turned off (e.g. for dev/localhost): `VERIFY_SSL=false locust -f locustfile_index-only.py`
# run/monitor test: http://localhost:8089/


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


# verify ssl requests (set to False if using localhost as the cert in this env is for prod)
verify_ssl = env_strtobool(os.environ.get("VERIFY_SSL", "true"))


class WebsiteTasksBasic(TaskSet):
    def on_start(self):
        # on_start is called when a Locust start before any task is scheduled
        # note: you can also add the verify=False to each client.get() client.post() if needed.
        self.client.verify = verify_ssl

    # ~ @task(1)
    # ~ def status(self):
    # ~ self.client.get("/api/status")

    @task(1)
    def form_post(self):
        access_key = "404ea561-df74-4be9-829c-ea3a5701a064"
        data = {
            "name": "Mr Testy Tester",
            "email": "mrtesty@test.com",
            "subject": "Load Testing",
            "message": "This is for load testing. We shall have more data here to test things.",
        }
        self.client.post(f"/api/v2/posts/form/{access_key}", data=data)


class WebsiteUser(HttpUser):
    host = "http://localhost:8029"

    print("\n\n############################################################")
    print(f"  NOTICE: VERIFY_SSL IS SET TO: {verify_ssl}")
    print(
        "    - if the host is using a self-signed/invalid cert you\n      MUST use VERIFY_SSL=false!"
    )
    print("############################################################\n\n")

    wait_time = between(5, 15)
    tasks = {WebsiteTasksBasic}
