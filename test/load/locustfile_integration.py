import os
import requests
import uuid
from locust import events

from locust import HttpUser, TaskSet, between, task

# install: `pip install locustio`
# start server: `locust -f locustfile_integration.py`
#  - to start with ssl verification turned off (e.g. for dev/localhost): `VERIFY_SSL=false locust -f locustfile_index-only.py`
# run/monitor test: http://localhost:8089/

#
# <====  STATUS  ====>
#  1. ✓simple post: (status/version)
#  2. ✓basic validate response (assert): (status/version)
#  3. authenticate: (/api/v2/auth/token)
#  4. use authentication to get data: (/api/v2/users/me)
#
#

token = ""  # nosec
runstamp = None

_test_app_user_username = os.getenv("TEST_APP_USER_USERNAME")
_test_app_user_password = os.getenv("TEST_APP_USER_PASSWORD")


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


@events.test_start.add_listener
def _(environment, **kwargs):
    global token
    token = get_token(environment.host)
    # print(f"- token is: {token}")
    global runstamp
    runstamp = uuid.uuid4()


def get_token(host):
    r = requests.post(
        host + "/api/v2/auth/token",
        headers={"content-type": "application/x-www-form-urlencoded"},
        data={
            "username": _test_app_user_username,
            "password": _test_app_user_password,
            "grant_type": "password",
        },
        timeout=5,
    )
    return r.json()["access_token"]


def assertValidStatusCode(resp: str = None, code: int = 200):
    with resp as r:
        if r.status_code != code:
            r.failure(f"Expected status code of: {code} but got: {r.status_code}")


def assertContains(response, text):
    with response as r:
        if text not in r.text:
            r.failure("Expected " + response.text + " to contain " + text)


# verify ssl requests (set to False if using localhost as the cert in this env is for prod)
verify_ssl = env_strtobool(os.environ.get("VERIFY_SSL", "true"))


class WebsiteTasksBasic(TaskSet):
    def on_start(self):
        # on_start is called when a Locust start before any task is scheduled
        # note: you can also add the verify=False to each client.get() client.post() if needed.
        self.client.verify = verify_ssl

    @task(0)
    def status(self):
        with self.client.get("/api/status", catch_response=True) as r:
            # asset works but it does not return a failure so you do not get failure counts - they show up in Exceptions
            # assert resp.json()["status"] == "okay", f"Unexpected value for status in response: {resp.json()["status"]}"
            _status = "OK"
            if r.json()["status"] != _status:
                r.failure(
                    f"Expected status of '{_status}' but got '{r.json()["status"]}'"
                )

    @task(0)
    def version(self):
        self.client.get("/api/version")
        assertValidStatusCode(self.client.get("/api/version", catch_response=True), 200)

    @task(0)
    def users_me(self):
        with self.client.get(
            "/api/v2/users/me",
            catch_response=True,
            headers={"Authorization": f"Bearer {token}"},
        ) as r:
            if r.json()["username"] != "tester":
                r.failure(
                    f"Expected username of 'tester' but got {r.json()['username']}"
                )

    @task(0)
    def users_get_all(self):
        with self.client.get(
            "/api/v2/users/",
            catch_response=True,
            json={"limit": 10, "skip": 0},
            headers={"Authorization": f"Bearer {token}"},
        ) as r:
            if r.json()[0]["username"] != "drad":
                r.failure(
                    f"Expected username of drad but got {r.json()[0]['username']}"
                )

    @task(0)
    def users_get_one(self):
        with self.client.get(
            "/api/v2/users/",
            catch_response=True,
            json={"username": "drad"},
            headers={"Authorization": f"Bearer {token}"},
        ) as r:
            # ~ print(f"- RESPONSE={r.json()}")
            if r.json()[0]["username"] != "drad":
                r.failure(
                    f"Expected username of drad but got {r.json()[0]['username']}"
                )

    @task(1)
    def users_update(self):
        with self.client.put(
            "/api/v2/users",
            catch_response=True,
            json={"username": "tester", "note": f"updated by locust: stamp={runstamp}"},
            headers={"Authorization": f"Bearer {token}"},
        ) as r:
            print(f"- RESPONSE={r.json()}")
            if r.json()["ok"] is not True:
                r.failure(f"Update of user failed: {r.json()}")

    # ~ @task(1)
    # ~ def form_post(self):
    # ~ access_key = "404ea561-df74-4be9-829c-ea3a5701a064"
    # ~ data = {
    # ~ "name": "Mr Testy Tester",
    # ~ "email": "mrtesty@test.com",
    # ~ "subject": "Load Testing",
    # ~ "message": "This is for load testing. We shall have more data here to test things.",
    # ~ }
    # ~ self.client.post(f"/api/v2/posts/form/{access_key}", data=data)


class WebsiteUser(HttpUser):
    host = "http://localhost:8029"

    print("\n\n############################################################")
    print(f"  NOTICE: VERIFY_SSL IS SET TO: {verify_ssl}")
    print(
        "    - if the host is using a self-signed/invalid cert you\n      MUST use VERIFY_SSL=false!"
    )
    print("############################################################\n\n")

    wait_time = between(5, 15)
    tasks = {WebsiteTasksBasic}
