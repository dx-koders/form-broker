# General

- scale up # of API: `dc up -d --no-deps --no-recreate --scale api=2 api`
- scale up # of Workers: `dc up -d --no-deps --no-recreate --scale worker=2 worker`
