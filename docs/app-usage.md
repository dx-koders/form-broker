# App Usage

### Initial App Setup

- there should be no 'initial' setup needed (users will be created upon login).

### Create Profile

- log into api
- add new profile (profiles > add)
  + make sure you get the returned access_key as your form submittal will need it!
- add desired dispatchers
- test via api (posts > POST Form)
  + you can use the Posts > Profile Count
  + if you added a db dispatcher you can use the messages > Get All


### CORS

- you need to specify the url that requests will come from in the CORS settings of fbkr api. If you want to allow localhost you would add 'http://localhost'.
