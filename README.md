# README

form-broker is an API that handles form submissions, sending the submission to configurable endpoints (email, database, etc.). form-broker was designed to handle any form submission and works well when combined with Static Site Generators such as [Nikola](https://getnikola.com/) or [Hugo](http://gohugo.io/) and also works well with traditional server-backed applications.


### Running

You can run your own instance of form-broker on kubernetes (see the k8s/helm section below) or via Docker (see `docker-compose.yml`).

Once you have an instance up, see [docs/app-usage](docs/app-usage.md) for how to set up a new profile.


###  More Info

See the `docs` directory.


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` per needs
- deploy the chart: `helm install form-broker form-broker/ --values form-broker/values.yaml.prod --create-namespace --namespace form-broker --disable-openapi-validation`
  - deploy update: `helm upgrade form-broker form-broker/ --values form-broker/values.yaml.prod --create-namespace --namespace form-broker --disable-openapi-validation`
  - uninstall: `helm uninstall -n form-broker form-broker`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n form-broker deployment worker api redis`
